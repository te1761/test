#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int theGame(int nbrE, int nbrM, int cpt, int rst, int embr)
{

int stab = rst;

        do
        {
            cpt+=1;

            // On demande le nombre
            printf("Quel est le nombre ? \n");
            //si embr = 2 alors on affiche cb d'essais il nous reste
            if(embr == 2)
            {
                printf("Il te reste %d essai(s)\n",rst);
                rst--;
            }

        scanf("%d", &nbrE);

            // On compare le nombre entré avec le nombre mystère

                if (nbrM > nbrE)
                    printf("C'est plus !\n\n");
                else if (nbrM < nbrE)
                    printf("C'est moins !\n\n");

            // Affiche le texte de victoire une fois gagné, embreillage fait la différence entre le mode lucky et les autres

                else if(nbrM == nbrE && (embr == 1 || embr == 2))
                    printf("\nBravo, vous avez trouve le nombre mystere en %d coups !!\n\n",cpt);
                else if(nbrM == nbrE && embr == 3)
                    printf("\nBravo, vous avez trouve le nombre mystere, tu as eu de la veine !\n\n");

            //si embr = 2 ou 3 et que le compteur arrive au niveau de stab -> Game Over

                    while((embr == 2 || embr == 3) && cpt == stab)
                    {
                        if(nbrE != nbrM)
                        {
                            printf("\n\nGame Over t'as perdu \nle resultat etait : %d\n\n\n", nbrM);
                        }
                            return(0);
                    }

        } while (nbrE != nbrM || cpt == stab);

return(0);
}

int main ( int argc, char** argv )
{
   //ContinuerPartie définit par 1 pour rentrer dans la boucle, à la fin de la partie le programme demande de choisir entre 1 pour continuer et 0 pour quitter le jeu.
    int continuerPartie = 1;

    while(continuerPartie == 1)
    {

        int nombreMystere = 0, nombreEntre = 0, compteur = 0, choixDifficultes = 10, reste, embreillage;
        int nombreMaximum, MIN = 1;

    //Ce while est présent pour empêcher le joueur du choisir une difficulté inexistante
        while(choixDifficultes >5 || choixDifficultes <1)
        {

        printf("\n\nChoisissez votre niveau de difficulte :\n\n");
        printf("Tapez 1 pour le niveau facile : entre 1 et 100\n");
        printf("Tapez 2 pour le niveau facile+ : entre 1 et 1000\n");
        printf("Tapez 3 pour le niveau intermediaire : entre 1 et 1000, avec 10 essais max\n");
        printf("Tapez 4 pour le niveau difficile : entre 1 et 1000, avec 7 essais max\n");
        printf("Tapez 5 pour le niveau lucky : entre 1 et 6, avec 1 essais max\n");
        printf("\n");
        scanf("%d", &choixDifficultes);

    //Le switch permet de set up le niveau en remplissant les variables selon la valeur de choixDifficulté

            switch(choixDifficultes)
            {
                case 1:
                    printf("\nniveau facile choisi\n\n");
                    nombreMaximum = 100;
                    embreillage = 1;
                break;
                case 2:
                    printf("\nniveau facile+ choisi\n\n");
                    nombreMaximum = 1000;
                    embreillage = 1;
                break;
                case 3:
                    printf("\nniveau intermediaire choisi\n\n");
                    nombreMaximum = 1000;
                    reste = 10;
                    embreillage = 2;
                break;
                case 4:
                    printf("\nniveau difficile choisi\n\n");
                    nombreMaximum = 1000;
                    reste = 7;
                    embreillage = 2;
                break;
                case 5:
                    printf("\nniveau lucky choisi\n\n");
                    nombreMaximum = 6;
                    reste = 1;
                    embreillage = 3;
                break;
                default:
                    printf("\nTu n'as pas choisi un bon niveau de difficulte , selectionne en un bon\n\n");
                break;
            }

        }


    // Génération du nombre aléatoire

srand(time(NULL));
nombreMystere = (rand() % (nombreMaximum - MIN + 1)) + MIN;

    //Appel de la fonction, toute les variable on été set up

theGame(nombreEntre, nombreMystere, compteur, reste, embreillage);

    printf("voulez-vous rejouer ?\n");
    printf("tapez 1 pour oui\n");
    printf("tapez 0 pour non\n");
    scanf("%d", &continuerPartie);

    //Ce while permet à l'utilisateur de ne pas mettre un chiffre différent que celui que l'on demande
        while(continuerPartie > 1 || continuerPartie < 0)
        {
                printf("vous n'avez pas bien selectionne le numero, ressayez\n");
                scanf("%d", &continuerPartie);
        }
    }
        printf("\n\nMerci d'avoir joue a mon jeu !\n\n\n");
        return(0);
    }








