Règles du jeu : trouver un nombre aléatoire définit entre un nombre minimum et maximum.
À chaque saisie, le jeu dira si le nombre aléatoire est inférieur ou supérieur à celui que l'on a saisi.

Il y a plusieurs niveaux de difficultés :


1 : facile.
2 : facile+.
3 : intermédiaire.
4 : difficile.
5 : lucky.

Le niveau 1 et 2 n'ont pas de limite d'essais.
Le niveau 3 et 4 ont une limite d'essais, et si l'on ne trouve pas le nombre aléatoire avant la limite définit, Game Over, et le jeu dit quel était le nombre.
Le niveau 5 met en jeu votre chance, on a le droit à un seul essai pour trouver un nombre entre 1 et 6.

À la fin de la partie, le jeu demande au joueur si il veut rejouer.
